package controllers

import java.io.File

import play.api._
import play.api.mvc._

class Application extends Controller {

  def index = Action {
    Ok(views.html.main("SandwichClub"))
  }

}
