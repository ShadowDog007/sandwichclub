package controllers.actions

import play.api.cache.CacheApi
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc._
import service.IAuthorisationService

import scala.concurrent.duration._
import scala.concurrent.{Promise, Future}
import scala.util.{Failure, Success}

object SecureAction {
  def apply[A](block: => Future[Result])(implicit cache: CacheApi, authService: IAuthorisationService) = new SecureActionBuilder().async(block)
  def apply[A](block: ((UserRequest[AnyContent]) => Future[Result]))(implicit cache: CacheApi, authService: IAuthorisationService) = new SecureActionBuilder().async(block)
  def apply[A](parser: BodyParser[A])(block: ((UserRequest[A]) => Future[Result]))(implicit cache: CacheApi, authService: IAuthorisationService) = new SecureActionBuilder().async(parser)(block)
}

/**
  * Created by Michael on 24/11/2015.
  */
class SecureActionBuilder(implicit cache: CacheApi, authService: IAuthorisationService)
  extends ActionBuilder[({ type R[A] = UserRequest[A] })#R] {

  def invokeBlock[A](request: Request[A], block: UserRequest[A] => Future[Result]): Future[Result] = {
  //def apply(request: Request[A])(implicit cache: CacheApi, authService: IAuthorisationService): Future[Result] = {
    val p = Promise[Result]

    request.headers.get("Sandwich-Auth-Token").map { token =>
      val cacheKey = s"authorisation $token"

      cache.get[Int](cacheKey) match {
        case Some(userId: Int) =>
          // Execute action
          p.completeWith(block(new UserRequest[A](userId, request)))
        case _ =>
          // Check auth tokens
          authService.authorise(token).andThen {
            case Success(userId) =>
              // Valid token, store the user id
              cache.set(cacheKey, userId, 10 minutes)
              p.completeWith(block(new UserRequest[A](userId, request)))
            case Failure(e) =>
              p.success(Results.Forbidden("Invalid 'Sandwich-Auth-Token'"))
          }
      }
    } getOrElse {
      // No header supplied
      p.success(Results.Unauthorized("Must provide 'Sandwich-Auth-Token' header"))
    }

    p.future
  }

  //lazy val parser = block.parser
}

case class UserRequest[A](val userId: Int, request: Request[A]) extends WrappedRequest[A](request)