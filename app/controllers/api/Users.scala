package controllers.api

import java.util.NoSuchElementException
import javax.inject.{Inject, Singleton}

import controllers.actions.SecureAction
import controllers.model.{User, UserFormats}
import data.model.{User => DataUser}
import data.repository.IUserRepository
import play.api.cache.CacheApi
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import service.IAuthorisationService

import scala.concurrent.{Future, Promise}

/**
  * Created by Michael on 24/11/2015.
  */
@Singleton
class Users @Inject()(private implicit val cacheApi: CacheApi,
                      private implicit val authorisationService: IAuthorisationService,
                      private implicit val userRepository: IUserRepository) extends Controller {

  import UserFormats._
  import Users._

  def getCurrentUser = SecureAction { request =>
    // Get current user
    userRepository.getUser(request.userId)
      // Check if the user exists
      .filter(_ != null)
      // Map to DTO
      .map(toDto)
      // 200 - Okay
      .map((user: User) => Ok(Json.toJson(user)))
      .recover {
        // No such user
        case e: NoSuchElementException => NotFound
      }
  }

  def getUser(id: Int) = SecureAction { request =>
    // Get current user
    userRepository.getUser(id)
      // Check if the user exists
      .filter(_ != null)
      // Map to DTO
      .map(toDto)
      // 200 - Okay
      .map((user: User) => Ok(Json.toJson(user)))
      .recover {
        // No such user
        case e: NoSuchElementException => NotFound
      }

  }

  def getUsers = SecureAction { request =>
    userRepository.getUsers()
      // Map to DTO
      .map(_.map(toDto))
      // 200 - Okay
      .map((users: Seq[User]) => Ok(Json.toJson(users)))
  }

  def getShoppers = SecureAction { request =>
      userRepository.getShoppers()
        // Map to DTO
        .map(_.map(toDto))
        // 200 - Okay
        .map((users: Seq[User]) => Ok(Json.toJson(users)))
  }

  def updateCurrentUser = SecureAction(BodyParsers.parse.json) { request =>
    val p = Promise[Result]

    // Validate the body
    request.body.validate[User] match {
      // Valid user object
      case JsSuccess(user, path) =>
        // Get the user
        userRepository.getUser(request.userId)
          .collect({
            case data: DataUser =>
              // Create updated object
              val updated = DataUser(user.firstName, user.lastName, user.email, user.avatarUrl, user.shopper, user.bankDetails, user.phoneNumber, user.bankName, user.firstLogin, data.fbId, data.id)
              // Persist changes
              val futureUpdate = userRepository.updateUser(updated)
                // Return Ok
                .map(_ => Ok)
              // Wait for update to complete
              p.completeWith(futureUpdate)
          })
          .recover {
            // No such user
            case e: NoSuchElementException => NotFound
          }
      case e: JsError => p.success(BadRequest("Invalid format =>\n" + JsError.toJson(e)))
    }

    p.future
  }

  def incrementSlices = SecureAction {
    Future.successful(NotImplemented)
  }

  def decrementSlices = SecureAction {
    Future.successful(NotImplemented)
  }
}

object Users {
  def toDto(user: DataUser): User = User(user.id.get, user.firstName, user.lastName, user.email, user.avatarUrl, user.shopper, user.bankDetails, user.phoneNumber, user.bankName, user.firstLogin)

  def toData(user: User): DataUser = toData(user)

  def toData(user: User, id: Option[Int] = None, fbId: Option[Int] = None): DataUser = DataUser(user.firstName, user.lastName, user.email, user.avatarUrl, user.shopper, user.bankDetails, user.phoneNumber, user.bankName, user.firstLogin)
}
