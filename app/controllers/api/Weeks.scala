package controllers.api

import javax.inject.{Inject, Singleton}

import controllers.actions.SecureAction
import controllers.model._
import data.model.{Week => DataWeek, WeekUserLink => DataWeekUserLink, User => DataUser}
import data.repository.{IUserRepository, IWeekUserLinkRepository, IWeekRepository}
import play.api.cache.CacheApi
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import service.IAuthorisationService

import scala.concurrent.Future
import scala.util.Success

/**
  * Created by Michael on 24/11/2015.
  */
@Singleton
class Weeks @Inject()(implicit private val cacheApi: CacheApi,
                      implicit private val authorisationService: IAuthorisationService,
                      weekRepository: IWeekRepository,
                      weekUserLinkRepository: IWeekUserLinkRepository,
                      userRepository: IUserRepository) extends Controller {

  import Weeks._
  import WeekFormats._
  import data.model.WeekUtil._

  def getCurrentWeek = getWeek(currentWeek)

  def getWeek(weekId: Int) = SecureAction {
    // Get week
    weekRepository.getWeek(weekId)
      // Check if week exists in database
      .filter(_ != null)
      .flatMap({
        week =>
          weekUserLinkRepository.getWeekUserLinksByWeek(weekId)
            .flatMap(links => {
              Future.sequence(
                links.map(l => {
                  userRepository.getUser(l.userId)
                    .map(user => linkToDto(l, user))
                })
              )
            })
            .map(links => { Week(week.cost, week.shopper, week.week, links) })
      })
      .recoverWith({
        // No week in database
        case e: NoSuchElementException =>
          weekRepository.addWeek(DataWeek(0, None, weekId))
            //.recover({case _ => 0})
            .map(_ => Week(0, None, weekId, Seq()))
        case e => e.printStackTrace();println("cri"); Future.failed(e)
      })
      // 200 - Okay
      .map((week: Week) => {println(week); println(Json); Ok(Json.toJson(week))})
  }

  def getLink(weekId: Int) = SecureAction { request =>
    // Get link
    weekUserLinkRepository.getWeekUserLink(request.userId, weekId)
      .recover {
        case e: NoSuchElementException =>
          DataWeekUserLink(request.userId, weekId)
      }
      .flatMap(link => {
        userRepository.getUser(link.userId)
          .map(user => linkToDto(link, user))
      })
      .map(link => Ok(Json.toJson(link)))
  }

  def updateWeek(weekId: Int) = SecureAction(BodyParsers.parse.json) { request =>
    request.body.validate[UpdatedWeek] match {
      case JsSuccess(updatedWeek, path) =>
        weekRepository.getWeek(weekId)
          .recover {
            case e: NoSuchElementException => DataWeek(0, None, weekId)
          }
          .flatMap {
            week =>
              weekRepository.updateWeek(toData(updatedWeek))
          }
          .filter(_ == 1)
          .map(_ => Ok)
          .recover {
            case e: NoSuchElementException =>
              InternalServerError("Failed updating week")
          }
      case JsError(e) =>
        Future.successful(BadRequest("Invalid format =>\n" + JsError.toJson(e)))
    }
  }

  def updateLink(weekId: Int) = SecureAction(BodyParsers.parse.json) { request =>
    request.body.validate[UpdatedWeekUserLink] match {
      case JsSuccess(link, path) =>
        val dataLink = toData(weekId, request.userId, link)
        val delete = link.paid <= 0.01 && link.slices == 0

        (
          if (!delete) weekUserLinkRepository.updateLink(dataLink)
          else weekUserLinkRepository.deleteLink(request.userId, weekId)
        ).map(_ => Ok)
      case JsError(e) =>
        Future.successful(BadRequest("Invalid format =>\n" + JsError.toJson(e)))
    }
  }

  def payForWeek(week: Int) = SecureAction {
    Future.successful(NotImplemented)
  }

  def getWeeksByUserId(userId: Int) = SecureAction {
    _getWeeksByUser(userId)
  }

  def getWeeksByUser = SecureAction { r =>
    _getWeeksByUser(r.userId)
  }

  private def _getWeeksByUser(userId: Int) = {
      mapWeeks(
        weekUserLinkRepository.getWeekUserLinksByUser(userId)
          .map(_.map(_.weekId)).flatMap(weekRepository.getWeeks)
      )
  }

  private def mapWeeks(weeks: Future[Seq[DataWeek]]) = {
    weeks
      // Map to DTO
      .map(weekToDto)
      .flatMap({
        weeks =>
          Future.sequence(
            weeks.map(week => {
              // Get links to user
              weekUserLinkRepository.getWeekUserLinksByWeek(week.week)
                .flatMap(links => {
                  Future.sequence(
                    links.map(l => {
                      userRepository.getUser(l.userId)
                        .map(user => linkToDto(l, user))
                    })
                  )
                })
                .map(links => {
                  // Add WeekUserLinks to Week object
                  Week(week.cost, week.shopper, week.week, links)
                })
            })
          )
      })
      // 200 - Okay
      .map((weeks: Seq[Week]) => Ok(Json.toJson(weeks)))
  }

}

object Weeks {
  def weekToDto(week: DataWeek): Week = Week(week.cost, week.shopper, week.week, Seq())
  def weekToDto(weeks: Seq[DataWeek]): Seq[Week] = weeks.map(weekToDto)

  def linkToDto(link: DataWeekUserLink, user: data.model.User) = WeekUserLink(link.userId, link.paid, link.slices, user.firstName + ' ' + user.lastName, user.avatarUrl)

  def toData(week: UpdatedWeek): DataWeek = DataWeek(week.cost, week.shopper, week.week)
  def toData(weekId: Int, userId: Int, link: UpdatedWeekUserLink) = DataWeekUserLink(userId, weekId, link.paid, link.slices)
}