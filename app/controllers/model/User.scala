package controllers.model

import play.api.libs.json.{Format, Json}

/**
  * Created by Michael on 24/11/2015.
  */
case class User(id: Int, firstName: String, lastName: String, email: String, avatarUrl: String, shopper: Boolean, bankDetails: String, phoneNumber: String, bankName: String, firstLogin: Boolean)

object UserFormats {
  implicit val userFormats: Format[User] = Json.format[User]
}
