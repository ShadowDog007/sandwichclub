package controllers.model

import play.api.libs.json.{Format, Json}

/**
  * Created by Michael on 12/12/2015.
  */
case class Week(cost: Double, shopper: Option[Int], week: Int, sandwichers: Seq[WeekUserLink])
case class UpdatedWeek(shopper:Option[Int], cost: Double, week: Int)

case class WeekUserLink(user: Int, paid: Double, slices: Int, name: String, avatarUrl: String)
case class UpdatedWeekUserLink(paid: Double, slices: Int)

object WeekFormats {
  implicit val updatedWeekFormats: Format[UpdatedWeek] = Json.format[UpdatedWeek]
  implicit val updatedWeekUserLinkFormats: Format[UpdatedWeekUserLink] = Json.format[UpdatedWeekUserLink]

  implicit val weekUserLinkFormats: Format[WeekUserLink] = Json.format[WeekUserLink]
  implicit val weekFormats: Format[Week] = Json.format[Week]
}
