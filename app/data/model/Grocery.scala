package data.model

/**
 * Represents an item which we buy for Sandwich club
 */
case class Grocery(
                  // The name of the item
                  name: String,
                  // Amount we buy
                  amount: String,
                  // Approximate cost (for visuals)
                  cost: Double,
                  // True if item is needed weekly
                  weekly: Boolean,
                  // Construct additional pylons
                  need: Boolean,
                  // Id
                  id: Option[Int] = None)
