package data.model

case class User(
                // The name of the user
                firstName: String,
                lastName: String,
                // The users email
                email: String,
                // Url to users profile picture
                avatarUrl: String,
                // True if the user is volunteering to be on the roster for buying groceries
                shopper: Boolean,
                // The users bank account
                bankDetails: String,
                phoneNumber: String,
                bankName: String,
                firstLogin: Boolean,
                fbId: Option[String] = None,
                id: Option[Int] = None)
