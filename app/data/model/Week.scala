package data.model

import java.util.Date

/**
 * Links users to weeks they had sandwiches
 */
case class Week(
                 // The cost of groceries this week
                 cost: Double,
                 // The user who is buying groceries
                 shopper: Option[Int],
                 // Weeks since the beginning of computer time! (1970)
                 week: Int) {
  def dateOfWeek = WeekUtil.weekToDate(week)
}

object WeekUtil {
  def currentWeek = {
    val seconds = System.currentTimeMillis() / 1000.0
    val hours = seconds / 3600.0
    val days = hours / 24.0
    val weeks = days / 7.0
    Math.ceil(weeks).asInstanceOf[Int]
  }

  def weekToDate(week: Int) = {
    val days = week * 7
    val hours = days * 24
    val seconds = days * 3600
    val millis = seconds * 1000

    new Date(millis)
  }
}