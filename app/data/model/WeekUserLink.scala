package data.model

/**
 * Link to which weeks a user had sandwiches
 */
case class WeekUserLink(
                         userId: Int,
                         weekId: Int,
                         // The amount the user paid the grocery shopper
                         paid: Double = 0,
                         // Number of slices this week
                         slices: Int = 0)
