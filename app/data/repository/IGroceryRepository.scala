package data.repository

import data.model.Grocery

import scala.concurrent.Future

trait IGroceryRepository extends IRepository[Grocery] {
  /**
   * Gets all groceries
   * @return
   */
  def getGroceries(): Future[Seq[Grocery]]

  /**
   * Gets a specific grocery
   * @param name
   * @return
   */
  def getGrocery(name: String): Future[Grocery]

  /**
   * Adds a new grocery item
   * @param grocery
   * @return Success boolean
   */
  def addGrocery(grocery: Grocery): Future[Boolean]

  /**
   *
   * @param grocery
   * @return Number of updated rows
   */
  def updateGrocery(grocery: Grocery): Future[Int]
}
