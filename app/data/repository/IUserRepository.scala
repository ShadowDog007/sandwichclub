package data.repository

import data.model.User

import scala.concurrent.Future

trait IUserRepository extends IRepository[User] {
  /**
   * Gets a specific user
   * @param id
   * @return
   */
  def getUser(id: Int): Future[User]

  /**
    *
    * @param id
    * @return
    */
  def getUserByFacebookId(id: String): Future[User]

  /**
   * Gets all users
   * @return
   */
  def getUsers(): Future[Seq[User]]
  def getShoppers(): Future[Seq[User]]

  /**
   *
   * @param user
   * @return The users id
   */
  def addUser(user: User): Future[Int]

  /**
   *
   * @param user
   * @return Number of updated rows
   */
  def updateUser(user: User): Future[Int]
}
