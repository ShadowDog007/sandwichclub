package data.repository

import data.model.{WeekUserLink, Week}

import scala.concurrent.Future

trait IWeekRepository extends IRepository[Week] {
  /**
   * Get all weeks a user has bought groceries
   * @param userId
   * @return
   */
  def getWeeksByShopper(userId: Int): Future[Seq[Week]]

  /**
   * Gets a specified week
   * @param week
   * @return
   */
  def getWeek(week: Int): Future[Week]

  /**
    * Gets all the provided weeks
    * @param weeks
    * @return
    */
  def getWeeks(weeks: Seq[Int]): Future[Seq[Week]]

  /**
   *
   * @param week
   * @return
   */
  def addWeek(week: Week): Future[Int]

  /**
   *
   * @param week
   * @return
   */
  def updateWeek(week: Week): Future[Int]
}
