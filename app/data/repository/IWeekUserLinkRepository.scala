package data.repository

import data.model.WeekUserLink

import scala.concurrent.Future

/**
  * Created by Michael on 12/12/2015.
  */
trait IWeekUserLinkRepository {
  /**
    *
    * @param week
    * @return
    */
  def getWeekUserLinksByWeek(week: Int): Future[Seq[WeekUserLink]]

  /**
    *
    * @param user
    * @return
    */
  def getWeekUserLinksByUser(user: Int): Future[Seq[WeekUserLink]]

  /**
    *
    * @param user
    * @param week
    * @return
    */
  def getWeekUserLink(user: Int, week: Int): Future[WeekUserLink]

  def addLink(link: WeekUserLink): Future[Int]
  def updateLink(link: WeekUserLink): Future[Int]
  def deleteLink(user: Int, week: Int): Future[Int]
}
