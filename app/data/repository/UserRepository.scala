package data.repository

import javax.inject.{Inject, Singleton}

import data.model.User
import data.schema.UsersSchema
import play.api.libs.concurrent.Execution.Implicits._

import scala.concurrent.Future

/**
  * Created by Michael on 26/11/2015.
  */
@Singleton
class UserRepository @Inject()(protected val schema: UsersSchema) extends IUserRepository {
  import schema.driver.api._

  private val db = schema.db
  private val users = schema.users

  /**
    * Gets a specific user
    * @param id
    * @return
    */
  override def getUser(id: Int): Future[User] = {
    val q = users.filter(_.id === id)
    val a = q.result.head
    db.run(a)
  }

  /**
    *
    * @param id
    * @return
    */
  override def getUserByFacebookId(id: String): Future[User] = {
    val q = users.filter(_.fbId === id)
    val a = q.result.head
    db.run(a)
  }

  /**
    * Gets all users
    * @return
    */
  override def getUsers(): Future[Seq[User]] = {
    val a = users.result
    db.run(a)
  }

  override def getShoppers(): Future[Seq[User]] = {
    val q = users.filter(_.shopper === true)
    val a = q.result

    db.run(a)
  }

  /**
    *
    * @param user
    * @return The users id
    */
  override def addUser(user: User): Future[Int] = {
    val a = (users returning users.map(_.id)) += user
    db.run(a).map(_.get)
  }

  /**
    *
    * @param user
    * @return Number of updated rows
    */
  override def updateUser(user: User): Future[Int] = {
    val q = users.filter(_.id === user.id.get)
    val a = q.update(user)
    db.run(a)
  }
}
