package data.repository

import javax.inject.{Inject, Singleton}

import data.model.Week
import data.schema.WeeksSchema

import scala.concurrent.Future

/**
  * Created by Michael on 12/12/2015.
  */
@Singleton
class WeekRepository @Inject()(protected val schema: WeeksSchema) extends IWeekRepository {
  import schema.driver.api._

  private val db = schema.db
  private val weeks = schema.weeks

  /**
    * Get all weeks a user has bought groceries
    * @param userId
    * @return
    */
  override def getWeeksByShopper(userId: Int): Future[Seq[Week]] = ???

  /**
    * Gets a specified week
    * @param week
    * @return
    */
  override def getWeek(week: Int): Future[Week] = {
    val q = weeks.filter(_.week === week)
    val a = q.result.head
    db.run(a)
  }

  /**
    * Gets all the provided weeks
    * @param weekIds
    * @return
    */
  override def getWeeks(weekIds: Seq[Int]): Future[Seq[Week]] = {
    val q = weeks.filter(_.week inSet weekIds)
    val a = q.result
    db.run(a)
  }

  override def addWeek(week: Week): Future[Int] = {
    val a = weeks.insertOrUpdate(week)
    db.run(a)
  }

  override def updateWeek(week: Week): Future[Int] = {
    val a = weeks.insertOrUpdate(week)
    db.run(a)
  }
}
