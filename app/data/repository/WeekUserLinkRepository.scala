package data.repository

import javax.inject.{Inject, Singleton}

import data.model.WeekUserLink
import data.schema.WeekUserLinksSchema

import scala.concurrent.Future

/**
  * Created by Michael on 12/12/2015.
  */
@Singleton
class WeekUserLinkRepository @Inject()(protected val schema: WeekUserLinksSchema) extends IWeekUserLinkRepository {
  import schema.driver.api._

  val db = schema.db
  val links = schema.links
  /**
    *
    * @param week
    * @return
    */
  override def getWeekUserLinksByWeek(week: Int): Future[Seq[WeekUserLink]] = {
    val q = links.filter(l => l.week === week && l.paid > 0.0 && l.slices =!= 0)
    val a = q.result
    db.run(a)
  }

  /**
    *
    * @param user
    * @return
    */
  override def getWeekUserLinksByUser(user: Int): Future[Seq[WeekUserLink]] = {
    val q = links.filter(l => l.user === user && l.paid > 0.0 && l.slices =!= 0)
    val a = q.result
    db.run(a)
  }

  /**
    *
    * @param user
    * @param week
    * @return
    */
  override def getWeekUserLink(user: Int, week: Int): Future[WeekUserLink] = {
    val q = links.filter(_.user === user).filter(_.week === week)
    val a = q.result.head
    db.run(a)
  }

  override def addLink(link: WeekUserLink): Future[Int] = {
    val a = links.insertOrUpdate(link)
    db.run(a)
  }

  override def updateLink(link: WeekUserLink): Future[Int] = {
    val a = links.insertOrUpdate(link)
    db.run(a)
  }

  override def deleteLink(user: Int, week: Int): Future[Int] = {
    val a = links.filter(_.user === user).filter(_.week === week).delete
    db.run(a)
  }
}
