package data.schema

import slick.driver.JdbcProfile

trait BaseSchema[M] extends HasDatabaseConfigProvider[JdbcProfile]

