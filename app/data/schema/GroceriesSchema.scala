package data.schema

import javax.inject.{Inject, Singleton}
import data.model.Grocery
import play.api.db.slick.DatabaseConfigProvider

@Singleton
class GroceriesSchema @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends BaseSchema[Grocery] {
  import driver.api._

  val users = TableQuery[GroceriesTable]

  class GroceriesTable(tag: Tag) extends Table[Grocery](tag, "Groceries") {
    def id      = column[Option[Int]]("GroceryId", O.PrimaryKey, O.AutoInc)

    def name    = column[String]("Name")
    def amount  = column[String]("Amount")
    def cost    = column[Double]("Cost")
    def weekly  = column[Boolean]("Weekly")
    def need    = column[Boolean]("Need")

    def * = (name, amount, cost, weekly, need, id) <> (Grocery.tupled, Grocery.unapply)
  }
}
