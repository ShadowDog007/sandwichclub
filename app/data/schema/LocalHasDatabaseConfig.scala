package data.schema

import play.api.db.slick.DatabaseConfigProvider

import slick.backend.DatabaseConfig
import slick.profile.BasicProfile

/**
 * Override of play.api.db.slick.HasDatabaseConfig to make driver public
 */
trait HasDatabaseConfig[P <: BasicProfile] {
  protected val dbConfig: DatabaseConfig[P]
  final lazy val driver: P = dbConfig.driver
  final def db: P#Backend#Database = dbConfig.db
}

/**
 * Override of play.api.db.slick.HasDatabaseConfigProvider to make driver public
 */
trait HasDatabaseConfigProvider[P <: BasicProfile] extends HasDatabaseConfig[P] {
  protected val dbConfigProvider: DatabaseConfigProvider
  override final protected val dbConfig: DatabaseConfig[P] = dbConfigProvider.get[P]
}
