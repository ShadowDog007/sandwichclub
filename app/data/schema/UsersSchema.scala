package data.schema

import javax.inject.{Inject, Singleton}
import data.model.User
import play.api.db.slick.DatabaseConfigProvider

@Singleton
class UsersSchema @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends BaseSchema[User] {
  import driver.api._

  val users = TableQuery[UsersTable]

  class UsersTable(tag: Tag) extends Table[User](tag, "Users") {
    def id  = column[Option[Int]]("UserId", O.PrimaryKey, O.AutoInc)

    def firstName = column[String]("FirstName")
    def lastName = column[String]("LastName")
    def email = column[String]("Email")
    def avatarUrl = column[String]("AvatarUrl")
    def shopper = column[Boolean]("Shopper")
    def bankDetails = column[String]("BankDetails")
    def firstLogin = column[Boolean]("FirstLogin")
    def phoneNumber = column[String]("PhoneNumber")
    def bankName = column[String]("BankName")
    def fbId = column[Option[String]]("FacebookId")

    def * = (firstName, lastName, email, avatarUrl, shopper, bankDetails, phoneNumber, bankName, firstLogin, fbId, id) <> (User.tupled, User.unapply)
  }
}
