package data.schema

import javax.inject.{Inject, Singleton}
import data.model.WeekUserLink
import play.api.db.slick.DatabaseConfigProvider

@Singleton
class WeekUserLinksSchema @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends BaseSchema[WeekUserLink] {
  import driver.api._

  val links = TableQuery[WeekUserLinksTable]

  class WeekUserLinksTable(tag: Tag) extends Table[WeekUserLink](tag, "WeekUserLinks") {
    def user =    column[Int]("UserId", O.PrimaryKey)
    def week =    column[Int]("WeekId", O.PrimaryKey)

    def paid =    column[Double]("Paid")
    def slices =  column[Int]("Slices")

    def * = (user, week, paid, slices) <> (WeekUserLink.tupled, WeekUserLink.unapply)
  }
}
