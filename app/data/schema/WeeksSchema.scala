package data.schema

import javax.inject.{Singleton, Inject}
import data.model.Week
import play.api.db.slick.DatabaseConfigProvider

@Singleton
class WeeksSchema @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends BaseSchema[Week] {
  import driver.api._

  val weeks = TableQuery[WeeksTable]

  class WeeksTable(tag: Tag) extends Table[Week](tag, "Weeks") {
    def week          = column[Int]("WeekId", O.PrimaryKey)

    def shopper = column[Option[Int]]("ShopperUserId")

    def cost          = column[Double]("Cost")


    def * = (cost, shopper, week) <> (Week.tupled, Week.unapply)
  }
}
