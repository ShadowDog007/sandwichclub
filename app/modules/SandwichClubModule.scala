package modules

import com.google.inject.AbstractModule
import data.repository._
import service.{AuthorisationService, IAuthorisationService}

/**
  * Created by Michael on 26/11/2015.
  */
class SandwichClubModule extends AbstractModule {
  override def configure = {
    bind(classOf[IUserRepository]).to(classOf[UserRepository])
    bind(classOf[IWeekRepository]).to(classOf[WeekRepository])
    bind(classOf[IWeekUserLinkRepository]).to(classOf[WeekUserLinkRepository])
    bind(classOf[IAuthorisationService]).to(classOf[AuthorisationService])
  }
}
