package service

import javax.inject.{Inject, Singleton}

import data.model.User
import data.repository.IUserRepository
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json._

import scala.IllegalArgumentException
import scala.concurrent.{Promise, Future}
import scala.io.Source

/**
  * Created by Michael on 26/11/2015.
  */
@Singleton
class AuthorisationService @Inject()(userRepository: IUserRepository) extends IAuthorisationService {

  /**
    *
    * @param token
    * @return The current user id
    */
  override def authorise(token: String): Future[Int] = {
    val split = token.split(' ')

    if (split.length != 2) return Future.failed(new IllegalArgumentException("Invalid token"))

    val provider = split(0)
    val tokenPart = split(1)

    provider match {
      case "facebook" => authoriseWithFacebook(tokenPart)
      case _ => Future.failed(new IllegalArgumentException(s"No support for provider: $provider"))
    }
  }

  case class FacebookUser(first_name: String, last_name: String, email: String, timezone: Int, picture: JsObject, id: String)
  private implicit val facebookUserReads = Json.reads[FacebookUser]

  def authoriseWithFacebook(token: String): Future[Int] = {
    val p = Promise[Int]

    var url = s"https://graph.facebook.com/me?fields=first_name,last_name,email,timezone,picture&access_token=$token"
    val content = Source.fromURL(url).mkString
    val json = Json.parse(content)

    json.validate[FacebookUser] match {
      case JsSuccess(fbuser: FacebookUser, path) =>
        // Get the user by their facebook id
        val userId = userRepository.getUserByFacebookId(fbuser.id)
          // Filter nulls
          .filter(_ != null)
          // Map to id
          .map(_.id.get)
          .recoverWith({
            // If there is no user
            case _ =>
              val jsUrl = (fbuser.picture \ "data" \ "url").get
              val url = jsUrl.asInstanceOf[JsString].value
              // Add the user to the database
              userRepository.addUser(User(fbuser.first_name, fbuser.last_name, fbuser.email, url, false, "", "", "", true, Some(fbuser.id)))
          })

        p.completeWith(userId)

      case e: JsError =>
        p.failure(new IllegalArgumentException("Invalid token"))
    }

    p.future
  }
}
