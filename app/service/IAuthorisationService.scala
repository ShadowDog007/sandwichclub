package service

import scala.concurrent.Future

/**
  * Created by Michael on 24/11/2015.
  */
trait IAuthorisationService {
  /**
    *
    * @param token
    * @return The current user id
    */
  def authorise(token: String): Future[Int]
}
