# Users Schema

# --- !Ups

CREATE TABLE Users (
  UserId      INT NOT NULL AUTO_INCREMENT,
  FacebookId  VARCHAR(128),

  FirstName   VARCHAR(255) NOT NULL,
  LastName    VARCHAR(255) NOT NULL,
  Email       VARCHAR(255) NOT NULL,
  AvatarUrl   VARCHAR(255) NOT NULL,
  Shopper     BIT NOT NULL,
  BankDetails VARCHAR(255) NOT NULL,
  FirstLogin  BIT NOT NULL,
  PhoneNumber VARCHAR(255) NOT NULL,
  BankName    VARCHAR(255) NOT NULL,

  PRIMARY KEY (UserId)
);

CREATE TABLE Weeks (
  WeekId      INT NOT NULL,

  ShopperUserId      INT,

  Cost        FLOAT NOT NULL,

  PRIMARY KEY (WeekId),
  CONSTRAINT fk_shopper FOREIGN KEY (ShopperUserId) REFERENCES Users(UserId)
);

CREATE TABLE WeekUserLinks (
  UserId      INT NOT NULL,
  WeekId      INT NOT NULL,

  Paid        FLOAT,
  Slices      INT,

  PRIMARY KEY (UserId, WeekId),
  CONSTRAINT fk_user FOREIGN KEY (UserId) REFERENCES Users(UserId),
  CONSTRAINT fk_week FOREIGN KEY (WeekId) REFERENCES Weeks(WeekId)
);

CREATE TABLE Groceries (
  GroceryId   INT NOT NULL AUTO_INCREMENT,

  Name        VARCHAR(255) NOT NULL,
  Amount      VARCHAR(255) NOT NULL,
  Cost        FLOAT NOT NULL,
  Weekly      BIT NOT NULL,
  Need        BIT NOT NULL,

  PRIMARY KEY (GroceryId)
);

# --- !Downs

DROP TABLE Groceries;
DROP TABLE WeekUserLinks;
DROP TABLE Weeks;
DROP TABLE Users;
