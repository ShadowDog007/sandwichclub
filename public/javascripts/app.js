var app = angular.module('sandwichclub', ['ui.router', 'ngAnimate', 'restangular']);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {
        //
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/");
        $locationProvider.html5Mode(false);
        //
        // Now set up the states
        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: "/assets/partials/home.html"
            })
            .state('login', {
                url: "/login",
                template: "<login></login>"
            })
            .state('register', {
                url: "/register",
                template: "<register></register>"
            })
            .state('shopping', {
                url: "/shopping",
                templateUrl: "/assets/partials/shopping.html"
            });
    }]);

app.run(['$rootScope', '$window', '$state', 'UserService', 'Restangular',
    function($rootScope, $window, $state, userService, Restangular) {

        $rootScope.user = {};

        if (!FB.getAccessToken()) {
            // no logged user, we should be going to #login
            $state.go('login');
        } else if($state.current.name === 'login') {
            Restangular.setDefaultHeaders({'Sandwich-Auth-Token': 'facebook ' + FB.getAccessToken()});
            $state.go('home');
        } else {
            Restangular.setDefaultHeaders({'Sandwich-Auth-Token': 'facebook ' + FB.getAccessToken()});
        }

        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if (!userService.getUser() && toState.name !== 'login') {
                    event.preventDefault();
                    // no logged user, we should be going to #login
                    $state.go('login');
                } else if(!!userService.getUser() && toState.name === 'login') {
                    $state.go('home');
                }
            })

    }]);

window.fbAsyncInit = function() {
    // Executed when the SDK is loaded
    FB.init({
         appId: '1542347896056535',
        //appId: '1558339397790718',
        channelUrl: 'app/channel.html',
        status: true,
        cookie: true,
        xfbml: false,
        version: "v2.5"
    });


    FB.getLoginStatus(function (data) {
        angular.bootstrap(document, ['sandwichclub']);
    });
};
