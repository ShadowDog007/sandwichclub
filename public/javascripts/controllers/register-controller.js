app.controller('RegisterController',['$scope', 'UserService', 'Restangular', '$location', function(scope, userService, restangular, $location) {
    scope.user = userService.getUser();

    scope.yesActive = false;
    scope.noActive = false;

    scope.bank = {};
    scope.bank.phoneNumber = '';
    scope.bank.bankName = '';

    scope.yes = function() {
        scope.yesActive = true;
        scope.noActive = false;
    };

    scope.no = function() {
        scope.yesActive = false;
        scope.noActive = true;
    };

    var bankMapping = {
        '01': 'ANZ',
        '02': 'BNZ',
        '03': 'Westpac',
        '12': 'ASB',
        '15': 'TSB',
        '38': 'Kiwibank'
    }

    scope.onPaste = function(event) {
        var data = event.originalEvent.clipboardData.getData('text/plain');
        var parts = data.split('-');
        if(parts.length == 4) {
            scope.bank.bank = parts[0];
            scope.bank.branch = parts[1];
            scope.bank.account = parts[2];
            if(parts[3].length == 2) {
                parts[3] = '0' + parts[3];
            }
            scope.bank.suffix = parts[3];
            if(bankMapping[parts[0]]) {
                scope.bank.bankName = bankMapping[parts[0]];
            }
            event.preventDefault();
        }
    };

    scope.signup = function() {
        scope.user.shopper = scope.yesActive;
        scope.user.bankDetails = scope.bank.bank + '-' + scope.bank.branch + '-' + scope.bank.account + '-' + scope.bank.suffix;
        scope.user.phoneNumber = scope.bank.phoneNumber;
        scope.user.bankName = scope.bank.bankName;
        restangular.one('user').customPOST(scope.user).then(function(response) {
            $location.path('/');
        });
    }
}]);