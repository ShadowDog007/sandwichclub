app.directive('loginButton', ['Restangular', 'UserService', '$state', function(restangular, userService, $state) {
    return {
        link: function (scope) {
            scope.login = function() {
                if (FB.getLoginStatus()) {
                    authService.didLogin(FB.getLoginStatus())

                } else {
                    FB.login(function (result) {
                        console.log(result)
                        if (result.status === "connected") {
                            restangular.setDefaultHeaders({'Sandwich-Auth-Token': 'facebook ' + result.authResponse.accessToken});
                            restangular.one('user').get().then(function (response) {
                                userService.didLogin(response.plain());
                                if(response.firstLogin) {
                                    $state.go('register');
                                } else {
                                    $state.go('home');
                                }
                            });
                        }
                    }, {scope: 'public_profile,email'});
                }
            }
        },
        templateUrl: '/assets/partials/login-button.html'
    };
}]);