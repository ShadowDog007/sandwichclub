app.directive('login', ['$state', 'UserService', function($state, userService) {
    return {
        link: function() {
            if(userService.isLoggedIn()) {
                $state.go('home');
            }
        },
        templateUrl: '/assets/partials/login.html'
    };
}]);