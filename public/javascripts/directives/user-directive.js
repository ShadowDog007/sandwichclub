app.directive('user',['UserService', function(userService) {
    return {
        link: function(scope) {
            scope.user = userService.getUser();

            scope.logout = function() {
                FB.logout();
                localStorage.removeItem("user");
            }
        },
        templateUrl: '/assets/partials/user.html'
    }
}]);
