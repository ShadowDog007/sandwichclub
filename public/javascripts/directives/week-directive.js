app.directive('week',['WeekService', 'UserService', function(weekService, userService) {
    return {
        link: function(scope) {
            scope.shopper;

            scope.weekId = weekService.getCurrentWeekId();

            scope.thisWeekId = weekService.getCurrentWeekId();

            scope.loading = true;

            scope.hasSlices = false;
            scope.hasShopper = false;

            scope.sandwichPlan = "0";

            updateWeek();

            scope.getWeekText = function() {
                if(scope.weekId === scope.thisWeekId) {
                    return 'This Week';
                } else {
                    return convertDate(weekService.weekIdToDate(scope.weekId));
                }
            };

            function convertDate(inputFormat) {
                function pad(s) { return (s < 10) ? '0' + s : s; }
                var d = new Date(inputFormat);
                return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
            }

            scope.returnToThisWeek = function() {
                scope.weekId = scope.thisWeekId;
            };

            scope.nextWeek = function() {
                scope.weekId++;
                updateWeek();
            };

            scope.prevWeek = function() {
                scope.weekId--;
                updateWeek();
            };

            scope.updatePlan = function(plan) {
                //scope.loading = true;
                plan = parseInt(plan);
                if (plan !== 0 && plan !== 1 && plan !== 2) {
                    plan = 0;
                }

                weekService.updatePlan(scope.weekId, plan).then(updateWeek);
            };

            scope.setShopperAsUser = function() {
                var user = userService.getUser();

                weekService.setShopperAsUser(scope.weekId, user.id)
                updateWeek();
            };

            scope.shopperIsUser = function() {
                return scope.week.shopper === userService.getUser().id;
            };

            scope.updateCost = function() {
                weekService.setCost(scope.weekId, scope.week.cost);
            };

            function updateWeek() {
                scope.loading = true;
                weekService.getWeek(scope.weekId).then(function(week) {
                    scope.week = week;
                    scope.hasShopper = !!week.shopper;
                    if(week.shopper) {
                        userService.getSpecificUser(week.shopper).then(function (shopper) {
                            scope.shopper = shopper;
                        });
                    }
                    weekService.getSlicesForWeek(scope.weekId).then(function(slices) {
                        scope.loading = false;
                        scope.slices = slices;
                        console.log(slices);
                        scope.sandwichPlan = "" + slices.slices;
                        scope.hasSlices = slices !== 0;

                    });
                });

            }

            scope.addSlice = function() {
                weekService.addSlice(this.weekId).then(function(data) {
                    scope.week = data;
                });
            };

            scope.removeSlice = function() {
                if(scope.slices.slices <= 0) {
                    scope.slices.slices  = 0;
                    return;
                }
                weekService.removeSlice(this.weekId).then(function(data) {
                    scope.week = data;
                });
            };

            scope.setPaid = function(hasPaid) {
                weekService.payWeek(this.weekId, scope.slices.paid).then(function(data) {
                    scope.week = data;
                });
            };

        },
        templateUrl: '/assets/partials/week.html'
    }
}]);
