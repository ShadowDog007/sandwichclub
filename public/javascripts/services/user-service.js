app.service('UserService', ['Restangular', function(Restangular) {
    var service = this;
    var user;

    service.didLogin = function(newUser) {
        user = newUser;
        localStorage.setItem("user", JSON.stringify(user));
    };

    service.getUser = function() {
        if(!user) {
            // Try load from localStorage
            var loadedUser = JSON.parse(localStorage.getItem("user"));

            if(loadedUser) {
                user = loadedUser;
            }
        }

        return user;
    };

    service.updateUser = function(newUser) {
        user = newUser;
        localStorage.setItem("user", JSON.stringify(user));
    };

    service.isLoggedIn = function() {
        return !!service.getUser();
    };

    service.addSlice = function() {
        Restangular.one('user/slices/up').post()
    };

    service.removeSlice = function() {
        Restangular.one('user/slices/down').post()
    };

    service.getSpecificUser = function(userId) {
        return Restangular.one('users', userId).get();
    };

    return service;
}]);